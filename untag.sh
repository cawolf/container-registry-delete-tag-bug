#!/bin/bash
TAGGED_IMAGES="tag tag-with-suffix"

for TAGGED_IMAGE in $TAGGED_IMAGES
do
  echo "Deleting $TAGGED_IMAGE"
  curl -X DELETE --header "PRIVATE-TOKEN: $MY_TOKEN" https://gitlab.com/api/v4/projects/23958423/registry/repositories/1662220/tags/$TAGGED_IMAGE
  echo ""
done

echo "Done"
